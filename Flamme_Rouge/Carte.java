/**
 * class Carte
 */
public abstract class Carte{

/**
 * represente le numero de la carte
 */
  private int numero;
/**
 * indique si la face visible de la carte est la face recto ou verso
 */
  private boolean verso;

/**
 * Constructeur Carte
 *
 * @param num le numero de la carte
 */
  public Carte(int num){
    this.numero=num;
    this.verso=true;
  }

/**
 * @return le numero de la carte
 */
  public int getNumCarte(){
    return this.numero;
  }

/**
 * permet de changer le sens de la carte
 */
  public void RetournerCarte(){
    this.verso=!(verso);
  }
  
  /**
   * permet de savoir si la carte est du cote verso ou recto
   * @return la face de la carte
   */
  public boolean estVerso() {
	  return this.verso;
  }
  

/**
 * permet de retourner en chaine de caractere le numero de la carte
 *
 * @return le numero de la carte en chaine de caractere
 */
  public String toString(){
    return (Integer.toString(this.numero));
  }
}
