/**
 * class CarteEnergie herite de la class Carte
 */
public class CarteEnergie extends Carte{


/**
 * Constructeur CarteEnergie
 * qui utilise le constructeur parent de la classe Carte
 *
 * @param num le numero de la carte
 */
  public CarteEnergie(int num){
    super(num);
  }
  
}
