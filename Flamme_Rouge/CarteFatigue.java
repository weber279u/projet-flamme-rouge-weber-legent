/**
 * class CarteFatigue herite de la class Carte
 */
public class CarteFatigue extends Carte{

	/**
	 * numero d une carte fatigue, elles possedent toutes le meme numero
	 */
  public final static int NUM=2;

/**
 * Constructeur CarteFatigue
 * qui utilise le constructeur parent de la classe Carte
 */
  public CarteFatigue(){
    super(NUM);
    this.RetournerCarte();
  }
  
}
