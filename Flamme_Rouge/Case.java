/**
 * class Case
 */
public class Case{

/**
 * represente le cycliste potentiel de la case
 */
  private Cycliste c;
/**
 * indique si la case est une montee
 */
  private boolean montee;
/**
 * indique si la case est une descente
 */
  private boolean descente;
/**
 * indique si la case est la fin dune tuile
 */
  private boolean finTuile;
/**
 * indique si la case est une case de depart
 */
  private boolean depart;
/**
 * indique si la case est une case d arrivee
 */
  private boolean arrive;

/**
 * Constructeur Case
 *
 * @param variation si cest une montee, une descente, du plat
 * 								si la valeur est null, la case cree est une case plate
 */
  public Case(String variation){
    this.c=null;
    this.finTuile=false;
    this.depart=false;
    this.arrive=false;
    switch(variation){
      case "montee":
        this.montee=true;
        this.descente=false;
        break;
      case "descente":
        this.montee=false;
        this.descente=true;
        break;
      case "plat":
        this.montee=false;
        this.descente=false;
        break;
      default :
    	  	this.montee=false;
        this.descente=false;
        break;
    }
  }

/**
 * methode estMontee
 * @return this.montee
 */
  public boolean estMontee(){
    return this.montee;
  }
/**
 * methode estDescente
 * @return si cest une descente ou pas
 */
  public boolean estDescente(){
    return this.descente;
  }

/**
 * methode estPlat
 * @return si cest un plat ou pas
 */
  public boolean estPlat(){
    return (this.montee==false && this.descente==false);
  }

/**
 * methode ajouterCycliste
 * ajout dun cycliste
 *
 * @param c le cycliste que lon veut rajouter
 */
  public void ajouterCycliste(Cycliste c){
    this.c=c;
  }

/**
 * methode enleverCycliste
 * enleve le cycliste a cette case
 */
  public void enleverCycliste(){
    this.c=null;
  }

/**
 * methode retournerCycliste
 *
 * @return le cycliste qui est sur la case
 */
  public Cycliste retournerCycliste(){
    return this.c;
  }

/**
 * methode definirMontee definit si la case est une montee
 */
  public void definirMontee(){
    this.montee=true;
    this.descente=false;
  }

/**
 * definirDescente definit si la case est une descente
 */
  public void definirDescente(){
    this.descente=true;
    this.montee=false;
  }
  
/**
 * met la case en tant que fin de tuile  
 */
  public void estLaFinTuile() {
	  this.finTuile=true;
  }
  
/**
 * permet de mettre la case en tant que case de depart  
 */
  public void setDepart() {
	  this.depart=true;
  }
  
/**
 * permet de mettre la case en tant que case d arrive  
 */
  public void setArrive() {
	  this.arrive=true;
  }
  
/**
 * methode estDepart  
 * @return si la case est une case depart
 */
  public boolean estDepart() {
	  return this.depart;
  }
  
 /** 
  * 
  * @return si cest une ligne d arrive ou de depart
  */
  public boolean estLigne() {
	  return (this.depart==true || this.arrive==true);
  }
  
/**
 *   
 * @return si cest la fin de la tuile
 */
  public boolean estLaFinDeLaTuile() {
	  return this.finTuile;
  }

  
}
