/**
 * class Circuit
 */
public class Circuit{

/**
 * represente le nom du circuit
 */
  private String nomCircuit;
/**
 * represente toutes les tuiles du circuit, autrement dit, la composition du circuit
 */
  private Tuile[] circuit;
/**
 * nombre max de tuiles lorsque le circuit est l Avenue Corso Paseo
 */
  public final static int MAX_ACP=21;

/**
 * Constructeur du Circuit
 * construit un circuit en fonction de son nom
 *
 * @param nom2Circuit le nom du circuit
 */
  public Circuit(String nom2Circuit){
    this.nomCircuit=nom2Circuit;
    switch(nom2Circuit){
      case "Avenue Corso Paseo":
          this.circuit=new Tuile[MAX_ACP];
          this.creerCircuitACP();
          break;
    }
    this.circuit[0].setDepart();
    this.circuit[this.circuit.length-2].setArrive();
  }

/**
 * permet d inserer les tuiles pour le circuit Avenue Corso Paseo
 */
  private void creerCircuitACP(){
    this.circuit[0]=TuileLigneDroite.A;
    this.circuit[1]=TuileLigneDroite.B;
    this.circuit[2]=TuileLigneDroite.C;
    this.circuit[3]=TuileLigneDroite.D;
    this.circuit[4]=TuileVirage.E;
    this.circuit[5]=TuileLigneDroite.F;
    this.circuit[6]=TuileVirage.G;
    this.circuit[7]=TuileVirage.H;
    this.circuit[8]=TuileVirage.I;
    this.circuit[9]=TuileVirage.J;
    this.circuit[10]=TuileVirage.K;
    this.circuit[11]=TuileLigneDroite.L;
    this.circuit[12]=TuileLigneDroite.M;
    this.circuit[13]=TuileLigneDroite.N;
    this.circuit[14]=TuileVirage.O;
    this.circuit[15]=TuileVirage.P;
    this.circuit[16]=TuileVirage.Q;
    this.circuit[17]=TuileVirage.R;
    this.circuit[18]=TuileVirage.S;
    this.circuit[19]=TuileVirage.T;
    this.circuit[20]=TuileLigneDroite.U;
  }

/**
 * permet d acceder a la composition du circuit
 *
 * @return toutes les tuiles du circuit
 */
  public Tuile[] getComposition(){
    return this.circuit;
  }


/**
 * permet d obtenir le nom du circuit
 * @return le nom du circuit
 */
public String getNomCircuit() {
	return nomCircuit;
}

/**
 * permet d obtenir le circuit
 * @return
 */
public Tuile[] getCircuit() {
	return circuit;
}

/**
 * permet de trouver la permiere case libre dans la tuile de depart
 * @return le premiere case libre dans la tuile de depart
 */
public int trouverPremierePlaceLibre() {
	int i=5;
	boolean fin=false;
	while(i>0 && fin==false) {
		if (this.circuit[0].getCases()[i].retournerCycliste()==null) {
			fin=true;
		}
		else {
			i--;
		}
	}
	return i;
}

/**
 * permet dajouter un cycliste a sa position sur le circuit
 * @param c le cycliste
 */
public void placerCycliste(Cycliste c) {
	this.circuit[c.getPositionTuile()].getCases()[c.getPositionCase()].ajouterCycliste(c);
}

/**
 * permet de supprimer l ancienne position du cycliste
 * @param c le cycliste
 */
public void supprimerCycliste(Cycliste c) {
	this.circuit[c.getPositionTuile()].getCases()[c.getPositionCase()].enleverCycliste();
}


/**
 * permet d afficher le circuit
 */
public void afficher() {
	String Ligne1="|";
	String Ligne2="|";
	String Ligne3="|";
	String Ligne4="|";
	String Ligne5="|";
	
	for (int i=0 ; i<this.getCircuit().length ; i++) {
		for (int j=0 ; j<this.getCircuit()[i].getCases().length ; j++) {
			
			if (this.getCircuit()[i].getCases()[j].retournerCycliste()==null) {	
				if (this.getCircuit()[i].getCases()[j].estLigne()) {
					Ligne1 = Ligne1 + "============//";
					Ligne2 = Ligne2 + "            //";
					Ligne3 = Ligne3 + "            //";
					Ligne4 = Ligne4 + "            //";
					Ligne5 = Ligne5 + "============//";
				}
				else {
					if (this.getCircuit()[i].getCases()[j].estLaFinDeLaTuile()) {
						if (this.getCircuit()[i].getCases()[j].estMontee()) {
							Ligne1 = Ligne1 + "<<<<<<<<<<<<<|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "<<<<<<<<<<<<<|";
						}
						if (this.getCircuit()[i].getCases()[j].estDescente()) {
							Ligne1 = Ligne1 + ">>>>>>>>>>>>>|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + ">>>>>>>>>>>>>|";
						}
						if (this.getCircuit()[i].getCases()[j].estPlat()) {
							Ligne1 = Ligne1 + "=============|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "=============|";
						}
					}
					else {
						if (this.getCircuit()[i].getCases()[j].estMontee()) {
							Ligne1 = Ligne1 + "<<<<<<<<<<<<<<";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "<<<<<<<<<<<<<<";
						}
						if (this.getCircuit()[i].getCases()[j].estDescente()) {
							Ligne1 = Ligne1 + ">>>>>>>>>>>>>>";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + ">>>>>>>>>>>>>>";
						}
						if (this.getCircuit()[i].getCases()[j].estPlat()) {
							Ligne1 = Ligne1 + "==============";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "             |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "==============";
						}
					}
				}
			}
			else {
				if (this.getCircuit()[i].getCases()[j].estLigne()) {
					Ligne1 = Ligne1 + "============//";
					Ligne2 = Ligne2 + "            //";
					Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")    //";
					Ligne4 = Ligne4 + "            //";
					Ligne5 = Ligne5 + "============//";
				}
				else {
					if (this.getCircuit()[i].getCases()[j].estLaFinDeLaTuile()) {
						if (this.getCircuit()[i].getCases()[j].estMontee()) {
							Ligne1 = Ligne1 + "<<<<<<<<<<<<<|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "<<<<<<<<<<<<<|";
						}
						if (this.getCircuit()[i].getCases()[j].estDescente()) {
							Ligne1 = Ligne1 + ">>>>>>>>>>>>>|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + ">>>>>>>>>>>>>|";
						}
						if (this.getCircuit()[i].getCases()[j].estPlat()) {
							Ligne1 = Ligne1 + "=============|";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "=============|";
						}
					}
					else {
						if (this.getCircuit()[i].getCases()[j].estMontee()) {
							Ligne1 = Ligne1 + "<<<<<<<<<<<<<<";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "<<<<<<<<<<<<<<";
						}
						if (this.getCircuit()[i].getCases()[j].estDescente()) {
							Ligne1 = Ligne1 + ">>>>>>>>>>>>>>";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + ">>>>>>>>>>>>>>";
						}
						if (this.getCircuit()[i].getCases()[j].estPlat()) {
							Ligne1 = Ligne1 + "==============";
							Ligne2 = Ligne2 + "             |";
							Ligne3 = Ligne3 + "    "+this.getCircuit()[i].getCases()[j].retournerCycliste().toString()+"("+this.getCircuit()[i].getCases()[j].retournerCycliste().getNumJoueur()+")     |";
							Ligne4 = Ligne4 + "             |";
							Ligne5 = Ligne5 + "==============";
						}
					}
				}
				
			}
		}
	}
	
	System.out.println(Ligne1);
	System.out.println(Ligne2);
	System.out.println(Ligne3);
	System.out.println(Ligne4);
	System.out.println(Ligne5);
}
    

}
