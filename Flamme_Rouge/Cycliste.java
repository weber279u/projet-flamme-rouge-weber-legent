import java.util.*;

/**
 * class Cycliste
 */
public abstract class Cycliste{

/**
 * represente le numero du cycliste
 */
  private int numCycliste;
/**
 * represente la position de tuile du cycliste
 */
  private int positionTuile;
/**
 * represente la position de case du cycliste
 */
  private int positionCase;
/**
 * represente les 4 cartes piochées lors de la phase d energie
 */
  private List<Carte> cartes;
/**
 * represente la carte piochée lors de la phase d energie
 */
  private int numCarte;
/**
 * indique si le cycliste a deja fait une aspiration durant le tour actuel
 */
  private boolean aspirationFaite;

/**
 * Constructeur Cycliste
 * 
 * @param num le numero du cycliste
 */
  public Cycliste(int num){
    this.numCycliste=num;
    this.positionTuile=0;
    this.positionCase=0;
    this.cartes=new ArrayList<Carte>();
    this.aspirationFaite=false;
  }

/**
 * methode piocher4Cartes
 * permet de priocher 4 cartes dans le paquet du joueur
 * 
 * @param le paquet du joueur
 */
 private void piocher4Cartes(List<Carte> paquet){
    for (int i=0 ; i<4 ; i++){
    	if (paquet.get(paquet.size()-1).estVerso()==false) {
    		Collections.shuffle(paquet);
    		for (int j=0 ; j<paquet.size() ; j++) {
    			paquet.get(j).RetournerCarte();
    		}
    		System.out.println("Joueur " + this.numCycliste + " vous êtes tombé sur une carte face visible, vous avez donc remélangé votre paquet et retourné toutes vos cartes");
    	}
      this.cartes.add(paquet.get(paquet.size()-1));
      paquet.remove(paquet.size()-1);
    }
  }

/**
 * methode choisirCarte
 * qui choisi une carte de la pioche de 4 cartes
 * 
 * @param le paquet du joueur
 */
 public void choisirCarte(List<Carte> paquet){
    Scanner sc=new Scanner(System.in);
    this.piocher4Cartes(paquet);
    System.out.println(this.afficherCartes());
    int num=sc.nextInt();
    int i=0;
    while (i<this.cartes.size() && num!=this.cartes.get(i).getNumCarte()) {
    		i++;
    }
    this.cartes.remove(i);
    for (int j=0 ; j<this.cartes.size() ; j++) {
    		this.cartes.get(j).RetournerCarte();
    }
    paquet.addAll(0, this.cartes);
    this.cartes.clear();
    this.numCarte=num;
  }
  

/**
 * @return le numero du joueur
 */
  public int getNumJoueur(){
    return this.numCycliste;
  }
  
/**
 * permet dafficher les differentes cartes que le joueur possede  
 * @return les differentes cartes dans une chaine de caractere
 */
  private String afficherCartes() {
	  String res="";
	  if (this instanceof Sprinteur) {
		  res="Joueur " + this.numCycliste + ", vous venez de piocher 4 cartes de votre paquet de cartes Sprinteur, vous disposez des cartes suivantes : ";
	  }
	  else {
		  res="Vous venez à présent de piocher 4 cartes de votre paquet de cartes Rouleur : ";
	  }
	  for (int i=0 ; i<this.cartes.size() ; i++) {
		  res = res + this.cartes.get(i).toString() + " ";
	  }
	  return res;
  }
  
/**
 * methode permettant de placer les cyclistes sur la tuile de depart
 * 
 * @param posCase la position du cycliste
 */
  public void placerCycliste(int posCase) {
	  this.positionCase=posCase;
  }

/**
 * methode getPositionTuile  
 * @return la position de tuile du cycliste
 */
public int getPositionTuile() {
	return positionTuile;
}

/**
 * permet de modifier la position de tuile du cycliste
 * @param positionTuile
 */
public void setPositionTuile(int positionTuile) {
	this.positionTuile = positionTuile;
}

/**
 * methode getPositionCase
 * @return la position de case du cycliste
 */
public int getPositionCase() {
	return positionCase;
}

/**
 * permet de modifier la position de case du cycliste
 * @param positionCase
 */
public void setPositionCase(int positionCase) {
	this.positionCase = positionCase;
}

/**
 * permet de faire avancer un cycliste
 * @param num le numero de la carte
 * @param c le circuit
 */
public void avancer(int num, Circuit c) {
	
	// si le cycliste est dans une descente
	if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estDescente()) {
		if (num<5) {
			num=5;
		}
	}
	// si le cycliste est dans une montee
	if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estMontee()) {
		if (num>5) {
			num=5;
		}
	}
	
	int i=0;
	boolean stop=false;
	while(i<num && !stop) {
		// si le cylciste est en fin de tuile
		if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estLaFinDeLaTuile()) {
			// si le cycliste est sur une case plate ou une descente
			if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estPlat() || c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estDescente()) {
				// si la case suivante est une montee
				if (c.getCircuit()[this.getPositionTuile()+1].getCases()[0].estMontee()) {
					// si le cycliste il ne peut pas engager la montee
					if (i==5) {
						stop=true;
					}
					// sinon il avance dune case
					else {
						this.setPositionTuile(this.getPositionTuile()+1);
						this.setPositionCase(0);
						i++;
					}
				}
				// sinon il avance dune case
				else {
					this.setPositionTuile(this.getPositionTuile()+1);
					this.setPositionCase(0);
					i++;
				}
			}
			// sinon le cycliste est sur une case montee
			else {
				// si la case suivante est une montee
				if (c.getCircuit()[this.getPositionTuile()+1].getCases()[0].estMontee()) {
					// si le cycliste il ne peut pas continuer la montee
					if (i>=5) {
						stop=true;
					}
					// sinon il avance dune case
					else {
						this.setPositionTuile(this.getPositionTuile()+1);
						this.setPositionCase(0);
						i++;
					}
				}
				// sinon il avance dune case
				else {
					this.setPositionTuile(this.getPositionTuile()+1);
					this.setPositionCase(0);
					i++;
				}
			}
		}
		else {
			// si le cycliste est sur une case plate ou une descente
			if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estPlat() || c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estDescente()) {
				// si la case suivante est une montee
				if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()+1].estMontee()) {
					// si le cycliste il ne peut pas engager la montee
					if (i>=5) {
						stop=true;
					}
					// sinon il avance dune case
					else {
						this.setPositionCase(this.getPositionCase()+1);
						i++;
					}
				}
				// sinon il avance dune case
				else {
					this.setPositionCase(this.getPositionCase()+1);
					i++;
				}
			}
			// sinon le cycliste est sur une case montee
			else {
				// si la case suivante est une montee
				if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()+1].estMontee()) {
					// si le cycliste il ne peut pas continuer la montee
					if (i>=5) {
						stop=true;
					}
					// sinon il avance dune case
					else {
						this.setPositionCase(this.getPositionCase()+1);
						i++;
					}
				}
				// sinon il avance dune case
				else {
					this.setPositionCase(this.getPositionCase()+1);
					i++;
				}
			}
		}
		
	}
			
	// tant qu il y a un cycliste sur cette place
	while (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].retournerCycliste()!=null) {
		// si l on doit retourner a la tuile d avant
		if (this.getPositionCase()==0) {			
			this.faireReculerCyclisteTuile(c);
		}
		else {
			this.faireReculerCyclisteCase(this.getPositionCase());
		}
	}
	
}

/**
 * permet de faire avancer en phase d aspiration sans les effets des montees et des descentes
 * @param c le circuit sur lequel les cyclistes se deplacent
 */
public void avancerAspiration(Circuit c) {
	if (c.getCircuit()[this.getPositionTuile()].getCases()[this.getPositionCase()].estLaFinDeLaTuile()) {
		this.setPositionTuile(this.getPositionTuile()+1);
		this.setPositionCase(0);
	}
	else {
		this.setPositionCase(this.getPositionCase()+1);
	}
}

/**
 * permet de faire reculer le cycliste d une case
 * @param pos la position de case
 */
private void faireReculerCyclisteCase(int pos) {
	this.setPositionCase(pos-1);
}

/**
 * permet de faire reculer d une tuile le cycliste
 * @param c le circuit du jeu
 */
private void faireReculerCyclisteTuile(Circuit c) {
	this.setPositionTuile(this.getPositionTuile()-1);
	this.setPositionCase(c.getCircuit()[this.getPositionTuile()].getCases().length-1);
}

/**
 * @return les cartes piochées lors de la phase d energie
 */
public List<Carte> getCartesCycliste() {
	return cartes;
}

/**
 * permet d acceder au numero de la carte qu a choisit le cycliste
 * @return le numero de la carte du cycliste
 */
public int getNumCarte() {
	return numCarte;
}

/**
 * @return indique si l aspiration a ete faite lors de ce tour
 */
public boolean getAspirationFaite() {
	return aspirationFaite;
}

/**
 * permet de modifier l attribut aspiration faite
 * @param aspirationFaite un booleen indiquant si laspiration a ete faite ou non
 */
public void setAspirationFaite(boolean aspirationFaite) {
	this.aspirationFaite = aspirationFaite;
}


}
