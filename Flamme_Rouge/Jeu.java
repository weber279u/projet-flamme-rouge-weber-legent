import java.util.*;

/**
 * class Jeu
 */
public class Jeu{

	/**
	 * represente la phase du jeu actuelle
	 */
  private String phaseDeJeu;
/**
 * indique si cest la fin du jeu ou non
 */
  private boolean fin;
/**
 * represente le circuit du jeu
 */
  private Circuit circuit;
/**
 * represente les joueurs du jeu
 */
  private Joueur[] joueurs;

/**
 * constructeur Jeu
 */
  public Jeu(){
    this.phaseDeJeu="Energie";
    this.fin=false;
    this.circuit=new Circuit("Avenue Corso Paseo");
    this.joueurs=null;
  }

/**
 * permet de demarrer le jeu
 * @param nbjoueurs le nombre de joueurs
 */
  public void demarrerJeu(int nbjoueurs){
	this.creerJoueur(nbjoueurs);
	this.positionDepart();
	System.out.println("Lancement de la partie avec " + this.joueurs.length + " joueurs, vous jouerez dans le circuit : " + this.getCircuit().getNomCircuit()+"\n");
	this.rappelDesRegles();
	this.afficherPositionCycliste();
	while (!this.estFini()) {
		switch(this.phaseDeJeu) {
		case "Energie":
			this.debutPhaseEnergie();
			break;
		case "Mouvement":
			this.debutPhaseMouvement();
			break;
		case "Finale":
			this.debutPhaseFinale();
			break;
		}
		if (this.finDeLaPartie()!=null) {
			this.fin=true;
			System.out.println("\nFIN DE LA PARTIE !\n-----------------\n\n" + this.joueurs[this.retourneCyclisteEnTete().getNumJoueur()-1].getNomJoueur() + " a gagner la partie, félicitation !");
		}
	}
  }

/**
 * met les cyclistes a la position de depart
 */
  public void positionDepart(){
	for (int i=0 ; i<this.joueurs.length ; i++) {
		int place1=this.circuit.trouverPremierePlaceLibre();
		// definit les positions du cycliste
		this.joueurs[i].getSprinteur().placerCycliste(place1);
		// qui permet de le placer physiquement sur le circuit
		this.circuit.placerCycliste(this.joueurs[i].getSprinteur());
		int place2=this.circuit.trouverPremierePlaceLibre();
		// meme chose pour le deuxieme cycliste
		this.joueurs[i].getRouleur().placerCycliste(place2);
		this.circuit.placerCycliste(this.joueurs[i].getRouleur());
	}
  }
  
/**
 * permet dajouter des joueurs a la partie  
 * @param nbjoueurs le nombres de joueurs
 */
  public void creerJoueur(int nbjoueurs) {
	  if (nbjoueurs>3) {System.out.println("3 joueurs maximum");nbjoueurs=3;}
	  Scanner sc=new Scanner(System.in);
	  this.joueurs=new Joueur[nbjoueurs];
	  for (int i=0 ; i<nbjoueurs ; i++) {
		  System.out.println("Entrez le nom du joueur " + (i+1) +" :");
		  String nom=sc.nextLine();
		  this.joueurs[i]=new Joueur(nom,(i+1));
	  }
  }

/**
 * permet de savoir si le jeu est fini
 *
 * @return si le jeu est fini
 */
  private boolean estFini(){
    return this.fin;
  }

  /**
   * permet dobtenir le circuit
   * @return
   */
public Circuit getCircuit() {
	return circuit;
}

/**
 * methode retourneCyclisteEnTete
 * @return le cycliste en tete de la course
 */
public Cycliste retourneCyclisteEnTete() {
	Cycliste c=null;
	boolean fin=false;
	int i=this.circuit.getCircuit().length-1;
	// parcours des differentes tuiles
	while (i>0 && fin==false) {
		int j=this.circuit.getCircuit()[i].getCases().length-1;
		// parcours des differentes cases
		while (j>0 && fin==false) {
			if (this.circuit.getCircuit()[i].getCases()[j].retournerCycliste()!=null) {
				c=this.circuit.getCircuit()[i].getCases()[j].retournerCycliste();
				fin=true;
			}
			else {
				j--;
			}
		}
		i--;
	}
	return c;
}


/**
 * permet de retrouver le premier cycliste qui na pas encore fais d aspiration derriere lui
 * @return le premier cycliste qui na pas encore fais d aspiration derriere lui
 */
public Cycliste retournerCyclistePremier() {
	Cycliste suiv=null;
	boolean fin=false;
	int i=0;
	// parcours des differentes tuiles
	while (i<this.circuit.getCircuit().length && fin==false) {
		int j=0;
		// parcours des differentes cases
		while (j<this.circuit.getCircuit()[i].getCases().length && fin==false) {
			if (this.circuit.getCircuit()[i].getCases()[j].retournerCycliste()!=null && this.circuit.getCircuit()[i].getCases()[j].retournerCycliste().getAspirationFaite()==false) {
				suiv=this.circuit.getCircuit()[i].getCases()[j].retournerCycliste();
				suiv.setAspirationFaite(true);
				fin=true;
			}
			else {
				j++;
			}
		}
		i++;
	}
	return suiv;
}



/**
 * permet de retrouver le cycliste derriere le cycliste faisant l aspiration
 * @return le cycliste derriere le cycliste faisant l aspiration
 */
public Cycliste retournerCyclisteSuivant() {
	Cycliste suiv=null;
	boolean fin=false;
	int i=0;
	// parcours des differentes tuiles
	while (i<this.circuit.getCircuit().length && fin==false) {
		int j=0;
		// parcours des differentes cases
		while (j<this.circuit.getCircuit()[i].getCases().length && fin==false) {
			if (this.circuit.getCircuit()[i].getCases()[j].retournerCycliste()!=null && this.circuit.getCircuit()[i].getCases()[j].retournerCycliste().getAspirationFaite()==false) {
				suiv=this.circuit.getCircuit()[i].getCases()[j].retournerCycliste();
				fin=true;
			}
			else {
				j++;
			}
		}
		i++;
	}
	return suiv;
}


/**
 * permet de faire piocher les joueurs un par un
 */
private void piocherCartes() {
	for (int i=0 ; i<this.joueurs.length ; i++) {
		this.joueurs[i].getSprinteur().choisirCarte(this.joueurs[i].getCartesSprinteur());
		this.joueurs[i].getRouleur().choisirCarte(this.joueurs[i].getCartesRouleur());
	}
}

/**
 * permet de passer a la phase suivante
 */
private void passerPhaseSuivante() {
	switch(this.phaseDeJeu) {
		case "Energie":
			this.phaseDeJeu="Mouvement";
			break;
		case "Mouvement":
			this.phaseDeJeu="Finale";
			break;
		case "Finale":
			this.phaseDeJeu="Energie";
			break;
	}
}

/**
 * permet de lancer la phase d energie
 */
private void debutPhaseEnergie() {
	System.out.println("\n\nPhase d'Énergie :\n");
	this.piocherCartes();
	this.passerPhaseSuivante();
}

/**
 * permet de lancer la phase de mouvement
 */
private void debutPhaseMouvement() {
	Scanner sc=new Scanner(System.in);
	System.out.println("\nPhase de Mouvement :\n\nVous retournez tous vos cartes et avancez vos Cyclistes en concéquence\n\n");
	for (int i=0 ; i<this.joueurs.length ; i++) {
		// faire avancer le Sprinteur du joueur i
		this.circuit.supprimerCycliste(this.joueurs[i].getSprinteur());
		this.joueurs[i].getSprinteur().avancer(this.joueurs[i].getSprinteur().getNumCarte(), this.circuit);
		this.circuit.placerCycliste(this.joueurs[i].getSprinteur());
		// faire avancer le Rouleur du joueur i
		this.circuit.supprimerCycliste(this.joueurs[i].getRouleur());
		this.joueurs[i].getRouleur().avancer(this.joueurs[i].getRouleur().getNumCarte(), this.circuit);
		this.circuit.placerCycliste(this.joueurs[i].getRouleur());
	}
	this.afficherPositionCycliste();
	if (this.retourneCyclisteEnTete() instanceof Sprinteur) {
		System.out.println("\nLe sprinteur du joueur n°"+this.retourneCyclisteEnTete().getNumJoueur()+" est en tête et lui sera donc attribué une carte fatigue à la phase suivante\n\nEntrez -continuer- pour passer a la phase finale");
	}
	else {
		System.out.println("\nLe rouleur du joueur n°"+this.retourneCyclisteEnTete().getNumJoueur()+" est en tête et lui sera donc attribué une carte fatigue à la phase suivante\n\nEntrez -continuer- pour passer a la phase finale");
	}
	String res=sc.nextLine();
	while(!res.equals("continuer")) {res=sc.nextLine();}
	this.passerPhaseSuivante();
}

/**
 * permet de lancer la phase finale
 */
private void debutPhaseFinale() {
	System.out.println("\nPhase Finale :\n\n");
	this.appliquerAspiration();
	this.appliquerFatigue();
	this.afficherPositionCycliste();
	this.passerPhaseSuivante();
}

/**
 * permet d afficher le circuit
 */
private void afficherPositionCycliste() {
	this.circuit.afficher();
}

/**
 * permet d expliquer les representations graphiques du jeu
 */
private void rappelDesRegles() {
	String regles="\n//                                  |                                                                   =============                      <<<<<<<<<<<<<                       >>>>>>>>>>>>>                                                                                         "
			     +"\n//                                  |                                 |                                                                                                                                                                                                              "
			     +"\n//  : Ligne d'arrivée/départ        |  : Séparation des tuiles        |  : Séparation des cases                        : Case plate                       : Case Montée                       : Case Descente        S(x)  : Sprinteur du joueur x        R(x)  : Rouleur du joueur x"
			     +"\n//                                  |                                 |                                                                                                                                                                                                              "
			     +"\n//                                  |                                                                   =============                      <<<<<<<<<<<<<                       >>>>>>>>>>>>>                                                                                         \n";
	System.out.println(regles);
}

/**
 * permet d appliquer l aspiration
 */
private void appliquerAspiration() {
	for (int i=0 ; i<this.joueurs.length*2 ; i++) {
		Cycliste c=this.retournerCyclistePremier();
		Cycliste suiv=this.retournerCyclisteSuivant();
		if (suiv!=null) {
			int posTuileC=c.getPositionTuile();
			int posTuileSuiv=suiv.getPositionTuile();
			int posCaseC=c.getPositionCase();
			int posCaseSuiv=suiv.getPositionCase();
			// si les 2 cyclistes sont dans la meme tuile et a 1 cases d intervalle, alors aspiration
			if (posTuileSuiv==posTuileC) {
				if ((posCaseSuiv-posCaseC)==2) {
					List<Cycliste> groupe=this.formerGroupe(c);
					for (int j=0 ; j<groupe.size() ; j++) {
						this.circuit.supprimerCycliste(groupe.get(j));
						groupe.get(j).avancerAspiration(this.circuit);
						this.circuit.placerCycliste(groupe.get(j));
					}
				}
			}
			// si les 2 cyclistes se trouvent dans une tuile differente mais juxtaposée
			if ((posTuileSuiv-posTuileC)==1) {
				if (this.compterCasesVide(c)==1) {
					List<Cycliste> groupe=this.formerGroupe(c);
					for (int j=0 ; j<groupe.size() ; j++) {
						this.circuit.supprimerCycliste(groupe.get(j));
						groupe.get(j).avancerAspiration(this.circuit);
						this.circuit.placerCycliste(groupe.get(j));
					}
				}
			}
		}
	}
	// remettre les booleens aspirationFaite a false
	for (int i=0 ; i<this.joueurs.length ; i++) {
		this.joueurs[i].getSprinteur().setAspirationFaite(false);
		this.joueurs[i].getRouleur().setAspirationFaite(false);
	}
}

/**
 * methode qui permet de retourner une liste de cycliste representant un groupe de cycliste
 * @param c le cycliste en tete du groupe (si il y en a un)
 * @return la liste de cycliste
 */
private List<Cycliste> formerGroupe(Cycliste c){
	List<Cycliste> liste=new ArrayList<Cycliste>();
	int posTuile=c.getPositionTuile();
	int posCase=c.getPositionCase();
	while (posTuile>-1 && this.circuit.getCircuit()[posTuile].getCases()[posCase].retournerCycliste()!=null) {
		liste.add(this.circuit.getCircuit()[posTuile].getCases()[posCase].retournerCycliste());
		if (posCase==0 && posTuile>0) {
			posTuile--;
			posCase=this.circuit.getCircuit()[posTuile].getCases().length-1;
		}
		else {
			posCase--;
		}
	}
	return liste;
}

/**
 * permet de compter le nombres de cases vides entre 2 cyclistes
 * @param c1 le premier cycliste
 * @return le nombres de cases entre eux 2
 */
private int compterCasesVide(Cycliste c1) {
	int nb=0;
	int i=0;
	int j=0;
	if (c1.getPositionCase()==this.circuit.getCircuit()[c1.getPositionTuile()].getCases().length-1) {
		i=c1.getPositionTuile()+1;
	}
	else {
		i=c1.getPositionTuile();
		j=c1.getPositionCase()+1;
	}
	while (this.circuit.getCircuit()[i].getCases()[j].retournerCycliste()==null) {
		nb++;
		if (j==this.circuit.getCircuit()[i].getCases().length-1 && i>0) {
			i++;
			j=0;
		}
		else {
			j++;
		}
	}
	return nb;
}

/**
 * permet dajouter une carte fatigue au cycliste en tete
 */
private void appliquerFatigue() {
	Cycliste c=this.retourneCyclisteEnTete();
	int joueur=c.getNumJoueur()-1;
	if (c instanceof Sprinteur) {
		this.joueurs[joueur].getCartesSprinteur().add(0,new CarteFatigue());
	}
	else {
		this.joueurs[joueur].getCartesRouleur().add(0,new CarteFatigue());
	}
}

/**
 * permet de savoir si la partie est fini ou pas
 * @return le cycliste le plus loin derriere la ligne darrivee
 */
private Cycliste finDeLaPartie() {
	Cycliste c=null;
	int i=this.circuit.getCircuit()[this.circuit.getCircuit().length-1].getCases().length-1;
	boolean stop=false;
	while (i>0 && !stop) {
		if (this.circuit.getCircuit()[this.circuit.getCircuit().length-1].getCases()[i].retournerCycliste()!=null) {
			stop=true;
			c=this.circuit.getCircuit()[this.circuit.getCircuit().length-1].getCases()[i].retournerCycliste();
		}
		else {
			i--;
		}
	}
	return c;
}



  
}
