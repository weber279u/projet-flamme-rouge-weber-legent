import java.util.*;

/**
 * class Joueur
 */
public class Joueur{

/**
 * represente le nom du joueur
 */
  private String nomJoueur;
/**
 * represente le numero de lequipe
 */
  private int numEquipe;
/**
 * represente le sprinteur du joueur
 */
  private Sprinteur sprinteur;
/**
 * represente le rouleur du joueur
 */
  private Rouleur rouleur;
/**
 * represente les cartes sprinteur du joueur
 */
  private List<Carte> cartesSprinteur;
/**
 * represente les cartes rouleur du joueur
 */
  private List<Carte> cartesRouleur;

/**
 * Constructeur Joueur
 *
 * @param nj une chaine representant le nom du joueur
 * @param ne un numero representant le numero de l equipe
 */
  public Joueur(String nj, int ne){
    this.nomJoueur=nj;
    this.numEquipe=ne;
    this.sprinteur=new Sprinteur(ne);
    this.rouleur=new Rouleur(ne);
    this.cartesSprinteur=new ArrayList<Carte>();
    this.cartesRouleur=new ArrayList<Carte>();
    this.remplirPaquetCartes();
    Collections.shuffle(this.cartesSprinteur);
    Collections.shuffle(this.cartesRouleur);
  }

  /**
   * permet d avoir le nom du joueur
   * @return le nom du joueur
   */
public String getNomJoueur() {
	return nomJoueur;
}

/**
 * permet d avoir le numero de lequipe
 * @return le numero de l equipe
 */
public int getNumEquipe() {
	return numEquipe;
}

/**
 * permet d acceder au sprinteur du joueur
 * @return le sprinteur du joueur
 */
public Sprinteur getSprinteur() {
	return sprinteur;
}

/**
 * permet d acceder au rouleur du joueur
 * @return le rouleur du joueur
 */
public Rouleur getRouleur() {
	return rouleur;
}

/**
 * permet de remplir les paquets de cartes des joueurs
 */
private void remplirPaquetCartes() {
	for (int i=0 ; i<4 ; i++) {
		this.cartesSprinteur.add(new CarteEnergie(2));
		this.cartesSprinteur.add(new CarteEnergie(3));
		this.cartesSprinteur.add(new CarteEnergie(4));
		this.cartesSprinteur.add(new CarteEnergie(5));
		this.cartesSprinteur.add(new CarteEnergie(9));
		this.cartesRouleur.add(new CarteEnergie(3));
		this.cartesRouleur.add(new CarteEnergie(4));
		this.cartesRouleur.add(new CarteEnergie(5));
		this.cartesRouleur.add(new CarteEnergie(6));
		this.cartesRouleur.add(new CarteEnergie(7));
	}
}

/**
 * @return le paquet de cartes sprinteur
 */
public List<Carte> getCartesSprinteur() {
	return cartesSprinteur;
}

/**
 * @return le paquet de cartes rouleur
 */
public List<Carte> getCartesRouleur() {
	return cartesRouleur;
}


}
