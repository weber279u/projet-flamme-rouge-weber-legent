import java.util.*;

public class Main{

/**
 * methode main
 */
  public static void main(String[] args) {
    // creation d un scanner
	Scanner sc=new Scanner(System.in);
	// demande du nombre de joueurs
	System.out.println("Combien de joueurs vont jouer ?");
	int nbj=sc.nextInt();
	// creation du jeu en fonction du nombre de joueurs
    Jeu jeu=new Jeu();
    jeu.demarrerJeu(nbj);
  }


}
