/**
 * class Rouleur herite de la class Cycliste
 */
public class Rouleur extends Cycliste{

/**
 * Constructeur Rouleur
 *
 * @param num le numero du cycliste
 */
  public Rouleur(int num){
    super(num);
  }

/**
 * permet de savoir si le cycliste est un Sprinteur
 * 
 * @return la chaine de caractere "R"
 */
  public String toString(){
    return ("R");
  }
}
