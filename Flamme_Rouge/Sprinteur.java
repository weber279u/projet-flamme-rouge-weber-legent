/**
 * class Sprinteur herite de la class Cycliste
 */
public class Sprinteur extends Cycliste{

/**
 * Constructeur Sprinteur
 *
 * @param num le numero du cycliste
 */
  public Sprinteur(int num){
    super(num);
  }

/**
 * permet de savoir si le cycliste est un Sprinteur
 * 
 * @return la chaine de caractere "S"
 */
  public String toString(){
    return ("S");
  }
}
