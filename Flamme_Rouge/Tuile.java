/**
 * class Tuile
 */
public abstract class Tuile{

/**
 * represente les cases quil y a dans une tuile
 */
  private Case[] tab;
/**
 * represente le nom de la tuile
 */
  private char nomTuile;

/**
 * Constructeur Tuile
 * initialisation dune tuile par defaut
 *
 * @param nbc le nombre de colonnes
 * @param c le nom de la tuile
 */
  public Tuile(int nbc, char c){
    this.tab=new Case[nbc];
    for (int i=0 ; i<nbc ; i++) {
    		this.tab[i]=new Case("plat");
    }
    this.tab[nbc-1].estLaFinTuile();
    this.nomTuile=c;
  }
  

/**
 * permet de mettre une montee dans une tuile
 *
 * @param debut le debut de la montee en terme de case
 * @param fin la fin de la montee en terme de case
 */
  public void setMontee(int debut, int fin){
    for (int i=debut ; i<fin+1 ; i++){
      this.tab[i].definirMontee();
    }
  }


  /**
   * permet de mettre une descente dans une tuile
   *
   * @param debut le debut de la descente en terme de case
   * @param fin la fin de la descente en terme de case
   */
    public void setDescente(int debut, int fin){
      for (int i=debut ; i<fin+1 ; i++){
        this.tab[i].definirDescente();
      }
    }
    
/**    
 * permet de mettre une tuile en tant que tuile de depart
 */
    public void setDepart() {
    		this.tab[this.tab.length-1].setDepart();
    }
    
/**
 * permet de mettre une tuile en tant que tuile d arrive    
 */
    public void setArrive() {
    		this.tab[this.tab.length-1].setArrive();
    }
    

/**
 * @return le nom de la tuile
 */
  public char getNomTuile(){
    return this.nomTuile;
  }

/**
 * @return les cases des tuiles
 */
  public Case[] getCases(){
    return this.tab;
  }


}
