/**
 * class TuileLigneDroite herite de Tuile
 */
public class TuileLigneDroite extends Tuile{

/**
 * le nomre de cases pour chaque ligne droite
 */
  public static final int NB_CASES=6;
/**
 * declaration des pieces que l'on aura besoin
 */
  public static final TuileLigneDroite A=new TuileLigneDroite('a');
  public static final TuileLigneDroite B=new TuileLigneDroite('b','m',3,5);
  public static final TuileLigneDroite C=new TuileLigneDroite('c',0,4,5,5);
  public static final TuileLigneDroite D=new TuileLigneDroite('d','d',0,2);
  public static final TuileLigneDroite F=new TuileLigneDroite('f');
  public static final TuileLigneDroite L=new TuileLigneDroite('l');
  public static final TuileLigneDroite M=new TuileLigneDroite('m');
  public static final TuileLigneDroite N=new TuileLigneDroite('n');
  public static final TuileLigneDroite U=new TuileLigneDroite('u');

/**
 * Constructeur TuileLigneDroite
 *
 * @param c le nom de la tuile
 */
  public TuileLigneDroite(char c){
    super(NB_CASES,c );
  }
  

/**
 * Constructeur utilisant la methode setMontee ou setDescente
 * 
 * @param c
 * @param debut
 * @param fin
 */
  public TuileLigneDroite(char c, char type, int debut, int fin) {
	  super(NB_CASES,c);
	  switch(type) {
	  	case 'm':
	  		this.setMontee(debut, fin);
	  		break;
	  	case 'd':
	  		this.setDescente(debut, fin);
	  		break;
	  }
  }
  
  
/**
 * constructeur qui permet de mettre une descente et une montee dans la meme tuile
 * 
 * @param c
 * @param mdebut
 * @param mfin
 * @param ddebut
 * @param dfin
 */
   public TuileLigneDroite(char c, int mdebut, int mfin, int ddebut, int dfin) {
	 super(NB_CASES,c);
	 this.setMontee(mdebut, mfin);
  	 this.setDescente(ddebut, dfin);
   }
}
