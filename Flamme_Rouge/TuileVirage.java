/**
 * class TuileVirage herite de Tuile
 */
public class TuileVirage extends Tuile{

/**
 * booleen precisant si le virage est serre ou leger
 */
  private boolean leger;
/**
 * le nombres de cases pour chaques virages
 */
  public static final int NB_CASES=2;
/**
 * declaration des differentes pieces virages necessaires
 */
  public static final TuileVirage E=new TuileVirage('e',false);
  public static final TuileVirage G=new TuileVirage('g',false);
  public static final TuileVirage H=new TuileVirage('h',true);
  public static final TuileVirage I=new TuileVirage('i',true);
  public static final TuileVirage J=new TuileVirage('j',true);
  public static final TuileVirage K=new TuileVirage('k',true);
  public static final TuileVirage O=new TuileVirage('o',false);
  public static final TuileVirage P=new TuileVirage('p',false);
  public static final TuileVirage Q=new TuileVirage('q',true);
  public static final TuileVirage R=new TuileVirage('r',true);
  public static final TuileVirage S=new TuileVirage('s',true);
  public static final TuileVirage T=new TuileVirage('t',true);

/**
 * Constructeur TuileVirage
 *
 * @param c le nom de la tuile
 * @param b si il est leger ou pas
 */
  public TuileVirage(char c, boolean b){
    super(NB_CASES,c);
    this.leger=b;
  }

/**
 * methode estLeger
 *
 * @return la valeur de l attribut leger
 */
  public boolean estLeger(){
    return this.leger;
  }


/**
 * methode estLeger
 *
 * @return la valeur contraire de l attribut leger
 */
  public boolean estSerre(){
    return !(this.leger);
  }
}
